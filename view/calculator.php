<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form method="post" action="?">
    <input type="number" name="num1" value="<?= isset($_GET['num1']) ? $_GET['num1'] : '' ?>" placeholder="Enter first number">
    <select name="operator">
        <option value="+" <?= (isset($_GET['operator']) && $_GET['operator'] === '+') ? 'selected' : ''; ?>> + </option>
        <option value="-" <?= (isset($_GET['operator']) && $_GET['operator'] === '-') ? 'selected' : ''; ?>> - </option>
        <option value="*" <?= (isset($_GET['operator']) && $_GET['operator'] === '*') ? 'selected' : ''; ?>> * </option>
        <option value="/" <?= (isset($_GET['operator']) && $_GET['operator'] === '/') ? 'selected' : ''; ?>> / </option>
    </select>
    <input type="number" name="num2" value="<?= isset($_GET['num2']) ? $_GET['num2'] : '' ?>" placeholder="Enter second number">
    <input type="submit">
</form>

<div id="output">
    <?php if(isset($_GET['result'])) : ?>
    <p>Result : </p>
    <h3><?= $_GET['result']; ?></h3>
    <?php endif; ?>
</div>

</body>
</html>
