<?php

$num1 = isset($_POST['num1']) ? $_POST['num1'] : 0;
$num2 = isset($_POST['num2']) ? $_POST['num2'] : 0;
$operator = isset($_POST['operator']) ? $_POST['operator'] : null;
$result = 0;

switch ($operator) {
    case '+':
        $result = Operation::add($num1, $num2);
        break;
    case '-':
        $result = Operation::subtract($num1, $num2);
        break;
    case '*':
        $result = Operation::multiply($num1, $num2);
        break;
    case '/':
        $result = Operation::divide($num1, $num2);
        break;
}

if ($result) {
    header('Location: index.php?num1=' . $num1 . '&num2=' . $num2 . '&operator=' . $operator . '&result=' . $result);
}
