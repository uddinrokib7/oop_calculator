<?php

class Operation {
    private function __construct() {

    }

    static function add($num1, $num2) {
        return $num1 + $num2;
    }

    static function subtract($num1, $num2) {
        return $num1 - $num2;
    }

    static function multiply($num1, $num2) {
        return $num1 * $num2;
    }

    static function divide($num1, $num2) {
        return round(($num1 / $num2), 2);
    }
}