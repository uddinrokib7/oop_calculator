<?php

/*
---------------------------------------------------------------------
| Define file directory of 'ROOT' path, 'CORE' path and 'VIEW' path |
---------------------------------------------------------------------
*/
defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
defined('ROOT') ? null : define('ROOT', __DIR__);
defined('CORE') ? null : define('CORE', ROOT . DS . 'core');
defined('VIEW') ? null : define('VIEW', ROOT . DS . 'view');
defined('URI_ROOT') ? null : define('URI_ROOT', $_SERVER['REQUEST_URI']);

/*
--------------------------------------------------------------------
| The Autoloader includes all classes from core/classes/ directory |
--------------------------------------------------------------------
 */
spl_autoload_register(function ($class) {
    $class = strtolower($class);
    require_once CORE . DS . $class . '.class.php';
});

/*
-----------------------------
| All PHP file requirements |
-----------------------------
 */
require_once VIEW . DS . 'calculator.php';
require_once VIEW . DS . 'calculation.php';
